# Site série
# Dépot git
https://gitlab.com/Nathan.Pigoreau/site-serie/-/tree/main#modal-upload-blob

## Acquisition du sujet

Lors de l'acquistion du sujet quand j'ai appris que je devais coder un site sur une série je me suis dit directement que j'allais le faire sur Tower of God car ce Webtoon me passion énormément et que je trouve qu'il y a beacoup de chose a dire sur ce sujet et donc beacoup de chose a essayer en html et css.

## Mes modifications personnels

Concernant ce que j'ai ajouter et qui n'était pas demander et les libertés que j'ai prise il y en a pas mal. Pour commencer la page personnage, j'ai décider de crée non pas une page personnage mais plusieurs car il y a tellement de personnage intéressant dans Tower of God je trouvais vraiment cela important, cependant cette réfléxions me pose un problème d'entrée de jeu comment faire un menu de navigations sans faire une liste de 20 items. J'en ai donc dédui que je devais apprendre a faire un menu déroulant avec le selecteur css :hover qui permet de définir une action quand l'utilisateur passe sa souris au déssus de l'élément mit en :hover. Grace a cela j'ai donc fait un menu de navigation qui se déroule lorsque l'on passe sa souris par dessus.

Ensuite concernant les pages et leurs contenus, dans les pages personnages j'ai décider de ne pas mettre de aside car je ne trouvais pas cela pertinent et ne savais pas quoi y mettre a l'intérieur cela aurrait donc était mettre des asides pour mettres des asides. J'ai don décider de mettre un aside dans l'accueil ou j'ai trouver l'idée de mettre une courte biographie de l'auteur de Tower of god ainsi que de son oeuvre principale.

Pour continuer sur mon menu de navigation j'ai trouver intéressant d'y rajouter des musiques avec la balise html audio, j'ai ajouter 4 musiques dans mon site qui sont directement accésible via le menu de navigation de mes pages.

Dans la page Fug on peut retrouver des liens menants a des id dans la pages qui permettent donc de naviguer a travers celle ci pour retrouver un élément plus rapidement.

## Conclusion 

Je pense que mon site respect les modalités imposer au départ, et je pense aussi y avoir ajouter ma touche personnel qui m'as permis de développer de nouvel compétence en html et css. De plus je pense continuer ce site tout au long de ma formation jusqu'à en être pleinement satisfait du résultat car pour le moment je le trouve encore basique.